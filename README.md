# vtt-hooks

Sample FoundryVTT module demonstrating functionality of Hooks.


```javascript

// Register callback for the next time hook is triggered only
Hooks.once("init", () => {
    // code here
})


// Declare callback function
let callback = () => {
    //code here
}

// Register callback for every time the hook is triggered
Hooks.on("hoverToken", callback)

// Unregister callback
Hooks.off("hoverToken", callback)
```