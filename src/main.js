/* ---------- Game Hooks ---------- */

// The Game object is about to go through initialisation
// This will register game settings, initialise i18n translations, setup listeners, etc.
Hooks.once("init", () => {
  console.log("Tutorial | init Hook - Game session initialisation started")
})

// The Game object is about to go through setup
// This will initialise Entities, Compendium Packs, UI, Keyboard, Sheets, etc.
Hooks.once("setup", () => {
  console.log("Tutorial | setup Hook - Game session setup started")
})

// The Game object has finished setup
Hooks.once("ready", () => {
  console.log("Tutorial | ready Hook - Game session setup is finished")
})

// The Game state has been toggled
Hooks.on("pauseGame", (paused) => {
  let state = paused ? "paused" : "resumed"
  console.log("Tutorial | pauseGame Hook - Game has been " + state)
})


/* ---------- Canvas Hooks ---------- */

// The Canvas is about to be drawn 
Hooks.on("canvasInit", (canvas) => {
  console.log(`Tutorial | canvasInit game Hook - Canvas drawing (grid size: ${canvas.scene.data.grid}) started`)
})

// The Canvas has been drawn
Hooks.on("canvasReady", (canvas) => {
  console.log(`Tutorial | canvasReady game Hook - Canvas drawing (grid size: ${canvas.scene.data.grid}) finished`)
})

// The Canvas has been panned to specific x,y and zoom level
Hooks.on("canvasPan", (canvas, constraints) => {
  console.log(`Tutorial | canvasPan game Hook - Canvas panning (x:${constraints.x}, y: ${constraints.y}, scale: ${constraints.scale}) finished`)
})


/* ---------- Application Hooks ---------- */

Hooks.on("renderCompendium", (compendium, html, data) => {
  console.log(`Tutorial | renderCompendium Hook - rendering Compendium: ${compendium.title}`)
})


/* ---------- PlaceableObject Hooks ---------- */

// These Hooks are available for all PlaceableObject types
// They are Token, Tile, Wall, Note, MeasuredTemplate, Drawing, AmbientLight, AmbientSound
// For example: hoverToken, hoverTile, hoverWall, etc.

// The Token is about to be created
Hooks.on("preCreateToken", (entity, data, options, userId) => {
  console.log(`Tutorial | preCreate Hook - User: ${userId} started creating Token: ${entity.name}`)
})

// The Token has been created
Hooks.on("createToken", (entity, options, userId) => {
  console.log(`Tutorial | createToken Hook - User: ${userId} finished creating Token: ${entity.name}`)
})

// The Token has been updated
Hooks.on("preUpdateToken", (entity, data, options, userId) => {
  console.log(`Tutorial | preUpdateToken Hook - User: ${userId} started updating Token: ${entity.name}`)
})

// The Token has been updated
Hooks.on("updateToken", (entity, data, options, userId) => {
  console.log(`Tutorial | updateToken Hook - User: ${userId} finished updating Token: ${entity.name}`)
})

// The Token is about to be deleted
Hooks.on("preDeleteToken", (entity, options, userId) => {
  console.log(`Tutorial | preDeleteToken Hook - User: ${userId} started deleting Token: ${entity.name}`)
})

// The Token has been deleted
Hooks.on("deleteToken", (entity, options, userId) => {
  console.log(`Tutorial | deleteToken Hook - User: ${userId} finished deleting Token: ${entity.name}`)
})

// The Token has been pasted
Hooks.on("pasteToken", (entity, data) => {
  console.log(`Tutorial | pasteToken Hook - pasting at ${data[0].x}, ${data[0].y}`)
})

// The Token's controlled state has changed
Hooks.on("controlToken", (placeableObject, controlled) => {
  let state = controlled ? "Controlling" : "Stopped controlling"
  console.log(`Tutorial | controlToken Hook - ${state} PlaceableObject: ${placeableObject.name}`)
})

// The Token's hover state has changed
Hooks.on("hoverToken", (placeableObject, hovered) => {
  let state = hovered ? "Hovered" : "Stopped hovering"
  console.log(`Tutorial | hoverToken game Hook - ${state} PlaceableObject: ${placeableObject.name}`)
})


/* ---------- User Hooks ---------- */

// The Player's targetted token state has changed
Hooks.on("targetToken", (user, token, targeted) => {
  let action = targeted ? "targeted" : "untargeted"
  console.log(`Tutorial | targetToken game Hook - User: ${user.name} has ${action} Token: ${token.name}`)
})


/* ---------- Controls Hooks ---------- */

// The Scene Controls have been initialised
Hooks.on("getSceneControlButtons", (sceneControls) => {
  console.log(`Tutorial | getSceneControlButtons game Hook - Scene Controls (First Control: ${sceneControls[0].name}) initialised`)
})

// The Scene Navigation menu state has changed
Hooks.on("collapseSceneNavigation", (sceneNavigation, collapsed) => {
  let state = collapsed ? "collapsed" : "expanded"
  console.log("Tutorial | collapseSceneNavigation game Hook - Scene Navigation menu " + state)
})

// The Sidebar state has changed
Hooks.on("sidebarCollapse", (sidebar, collapsed) => {
  let state = collapsed ? "collapsed" : "expanded"
  console.log("Tutorial | sidebarCollapse game Hook - Sidebar " + state)
})

// The Hotbar Slot is being assigned 
// Return false to stop the hotbar slot assignment
Hooks.on("hotbarDrop", (hotbar, data, slot) => {
  let state = collapsed ? "collapsed" : "expanded"
  console.log("Tutorial | hotbarDrop game Hook - Sidebar " + state)
})


/* ---------- Chat Hooks ---------- */

// The message to be spoken by a Token
// Return false to block message
Hooks.on("chatBubble", (token, html, message, emote) => {
  let state = emote ? "emoted" : "said"
  console.log(`Tutorial | chatBubble Hook - Token: ${token.name} ${state} "${message}" in a chat bubble`)
})

// The ChatMessage was rendered
Hooks.on("renderChatMessage", (chatMessage, html,   ) => {
  let state = chatMessage.isRoll ? "was" : "wasn't"
  console.log(`Tutorial | renderChatMessage Hook - Speaker: ${chatMessage.alias} message ${state} a dice roll`)
})

// The ChatMessage was entered by User
// Return false to block message
Hooks.on("chatMessage", (chatLog, message, chatData) => {
  console.log(`Tutorial | chatMessage Hook - User: ${chatData.user} as ${chatData.speaker} sent message: "${message}"`)
})


/* ---------- Context Hooks ---------- */

// The Scene has been changed
Hooks.on("getSceneNavigationContext", (html, contextOptions) => {
  console.log(`Tutorial | getSceneNavigationContext Hook - First ContextOption: ${contextOptions[0].name}`)
})

// The Players menu state has been changed
Hooks.on("getUserContextOptions", (html, contextOptions) => {
  console.log(`Tutorial | getUserContextOptions Hook - First ContextOption: ${contextOptions[0].name}`)
})

// SceneDirectory, ActorDirectory, ItemDirectory, JournalDirectory, RollTableDirectory
Hooks.on("getActorDirectoryFolderContext", (html, folderOptions) => {
  console.log(`Tutorial | getActorDirectoryFolderContext Hook - First FolderOption: ${folderOptions[0].name}`)
})

// SceneDirectory, ActorDirectory, ItemDirectory, JournalDirectory, RollTableDirectory
Hooks.on("getSceneDirectoryContext", (html, entryOptions) => {
  console.log(`Tutorial | getSceneDirectoryContext Hook -  First EntryOption: ${entryOptions[0].name}`)
})

// PlaylistDirectory
Hooks.on("getPlaylistDirectorySoundContext", (html, entryOptions) => {
  console.log(`Tutorial | getPlaylistDirectorySoundContext Hook - First EntryOption: ${entryOptions[0].name}`)
})


/* ---------- Networking Hooks ---------- */

// The WebRTC.Client connected to the WebRTC server 
Hooks.on("rtcClientConnected", (webRTC) => {
  console.log(`Tutorial | rtcClientConnected Hook - connected to server with host: ${webRTC.serverUrl}`)
})

// The WebRTC.Client disconnected from the WebRTC server 
Hooks.on("rtcClientDisconnected", (webRTC) => {
  console.log(`Tutorial | rtcClientDisconnected Hook - disconnected from server with host: ${webRTC.serverUrl}`)
})

// The WebRTC settings have been changed
Hooks.on("rtcSettingsChanged", (webRTCSettings) => {
  console.log("Tutorial | rtcSettingsChanged Hook - WebRTC settings changed")
})

// The media stream has been opened
Hooks.on("rtcLocalStreamOpened", (webRTC, stream) => {
  console.log("Tutorial | rtcLocalStreamOpened Hook - Local media stream opened")
})

// The media stream has been closed
Hooks.on("rtcLocalStreamClosed", (webRTC) => {
  console.log("Tutorial | rtcLocalStreamClosed Hook - Local media stream closed")
})

// The User's stream has changed
Hooks.on("rtcUserStreamChange", (webRTC, streamInfo) => {
  console.log(`Tutorial | rtcUserStreamChange Hook - Stream changed for User: ${streamInfo.userId}`)
})

// The Stream was created for User
Hooks.on("rtcCreateStreamForUser", (webRTC, userId, tracks) => {
  console.log("Tutorial | rtcCreateStreamForUser Hook - Local media stream closed")
})